#ifndef CSV_H
#define CSV_H

#include <stdio.h>

/**
 * CSV File handler
 * 
 * <p>Handler for using and referencing CSV Files.
 * To create one file please use the function csv_create_file().
 * 
 * @author Fabian Benschuh <Fabi.Benschuh@gmx.de>
 * @version 0-alpha
 * @since 0
 * @see csv_create_file()
 */ 
typedef struct
{
    FILE *file; /**< File pointer to the csv File */
    char delimiter; /**< Delimiter of the csv File */
    int columns; /**< Actual number of columns created for this file. DO NOT CHANGE IT! */
    int rows; /**< Current number of rows or lines in the File. DO NOT CHANGE IT! */
} csv;

/** 
 * Creates a csv struct when given a pointer as parameter _csv with a FILE Pointer
 * on the specified _path.
 * 
 * <p>The content of the file will be separated with the specified delimiter
 * and have the specified header included as a first line.
 * 
 * @param _csv Pointer to the output CSV File. The content of this pointer will be overwritten.
 * @param _path Absolute or relative path to the CSV File without the name which should be created.
 * @param name The name of the CSV File which should be created.
 * @param delimiter The delimiter which should be used inside this CSV file for the separation of the contents.
 * @param header The first line in the CSV file. Should have the appropriate number of columns.
 * @return
 * - 0 success
 * - -1 No memory
 * - -2 File system error
 * @since 0
 */
int csv_create_file(csv **_csv, const char *_path, const char *name, char delimiter, const char * const * header);

/**
 * Save and close a given CSV File.
 * 
 * @param _csv The CSV File Handler which should be closed.
 * @return
 * - 0 success
 * - -1 Null or wrong parameter
 * - -2 File system error
 * @since 0
 */
int csv_close_file(csv **_csv);

/**
 * Writes one line to the CSV File.
 * 
 * <p> Takes a list of strings as an argument and try to add it to the CSV file,
 * separated by the delimiter specified on creating.
 * @param csv The CSV File which should be modified.
 * @param row List of strings to be added to the CSV File.
 * @since 0
 */
int csv_write_row(csv *csv, const char **row);

#endif // CSV_H

#include "csv.h"
#include <stdio.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

int csv_create_file(csv **_csv, const char *_path, const char *name, char delimiter, const char * const * header)
{
    *_csv = NULL;
    *_csv = (csv*)malloc(sizeof(csv));
    if(*_csv == NULL)
    {
        fprintf(stderr, "malloc failed. Can't build csv file\n");
        return -1;
    }
    char *path = NULL;
    path = (char *)malloc(sizeof(char) * (strlen(_path) + 1 + strlen(name) + 4 + 1)); // len(_path) + '/' + len(name) + ".csv" + '\0'
    if(path == NULL)
    {
        fprintf(stderr, "malloc path failed. Can't build csv file path\n");
        return -1;
    }
    strcpy(path, _path);
    strcat(path, "/");
    errno = 0;
    int ret = mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IWOTH);
    if (ret == -1) {
        switch (errno) {
            case EACCES :
                fprintf(stderr, "the parent directory does not allow write the log files\n");
                return -1;
            case EEXIST:
                break;
            case ENAMETOOLONG:
                fprintf(stderr, "pathname for cvs is too long\n");
                return -1;
            default:
                fprintf(stderr, "mkdir: %s\n", strerror(errno));
                return -1;
        }
    }

    strcat(path, name);
    strcat(path, ".csv");
    (*_csv)->file = fopen(path, "a");
    if((*_csv)->file == NULL)
    {
        fprintf(stderr, "can't open file: %s", strerror(errno));
        return -1;
    }
    (*_csv)->delimiter = delimiter;
    if(ftell((*_csv)->file) == 0)
    {
        (*_csv)->columns = csv_write_row(*_csv, (const char **)header);
    }
    (*_csv)->rows = 0;
    return 0;
}

int csv_close_file(csv **_csv)
{
    int ret = -1;
    if((*_csv)->file != NULL)
    {
        ret = fclose((*_csv)->file);
    }
    if(*_csv != NULL)
    {
        free(*_csv);
    }
    *_csv = NULL;
    return ret;
}

int csv_write_row(csv *csv, const char **row)
{
    int columns = 0;
    for(const char **col = row; *col != NULL; col++)
    {
        fputs(*col, csv->file);
        fputc(csv->delimiter, csv->file);
        columns++;
    }
    fseek(csv->file, ftell(csv->file)-1, SEEK_SET);
    fputc('\n', csv->file);
    return columns;
}
